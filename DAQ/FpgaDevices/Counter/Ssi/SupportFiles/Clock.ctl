RSRC
 LVCCLBVW  -l  F      -L   � �0          < � @       ����            �Ira9E�Nn=�ǈ          B`HE*��J�V�\����ُ ��	���B~       ������I�~�36#r?   r)�ao��\��p���   �  11.0.1             LVCC	Clock.ctl          ������  �  �@ �� �� �  �� �� �� �� �� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��                    ��      +����                   ��   ��V                        ��       ���                    ��       ���                    ��       ���                    ��       ���                    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������          FPHP	Clock.ctl         J �               displayFilter �                   tdData �               ContainerInterface �    @0����name       �<Interface><DataType><Type>boolean</Type></DataType><MethodSet><Method name="Write"/><Method name="Read"/></MethodSet></Interface>       	typeClass �     0����      FPGA Memory        � �               displayFilter �                   tdData �               IOInterface �    @0����name      �<Interface>
<MethodSet>
   <Method name="Read">
      <LocalizedName localize="true">Read</LocalizedName>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <LocalizedName localize="true">Write</LocalizedName>
      <ParameterList>
         <Parameter name="Value">
            <LocalizedName localize="true">Value</LocalizedName>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O         �               displayFilter �                   tdData �               IOInterface �    @0����name      V<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName localize="true">Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues localize="true">Inherit From Project Item,Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
         </Attribute>
      </AttributeSet>
      <LocalizedName localize="true">Read</LocalizedName>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Wait on Any Edge">
      <LocalizedName localize="true">Wait On Any Edge</LocalizedName>
      <ParameterList>
         <Parameter name="Timeout">
            <Direction>in</Direction>
            <LocalizedName localize="true">Timeout</LocalizedName>
            <Required>no</Required>
            <Type>i32</Type>
         </Parameter>
         <Parameter name="Timed Out">
            <Direction>out</Direction>
            <LocalizedName localize="true">Timed Out</LocalizedName>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Wait on Falling Edge">
      <LocalizedName localize="true">Wait On Falling Edge</LocalizedName>
      <ParameterList>
         <Parameter name="Timeout">
            <Direction>in</Direction>
            <LocalizedName localize="true">Timeout</LocalizedName>
            <Required>no</Required>
            <Type>i32</Type>
         </Parameter>
         <Parameter name="Timed Out">
            <Direction>out</Direction>
            <LocalizedName localize="true">Timed Out</LocalizedName>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Wait on High Level">
      <LocalizedName localize="true">Wait On High Level</LocalizedName>
      <ParameterList>
         <Parameter name="Timeout">
            <Direction>in</Direction>
            <LocalizedName localize="true">Timeout</LocalizedName>
            <Required>no</Required>
            <Type>i32</Type>
         </Parameter>
         <Parameter name="Timed Out">
            <Direction>out</Direction>
            <LocalizedName localize="true">Timed Out</LocalizedName>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Wait on Low Level">
      <LocalizedName localize="true">Wait On Low Level</LocalizedName>
      <ParameterList>
         <Parameter name="Timeout">
            <Direction>in</Direction>
            <LocalizedName localize="true">Timeout</LocalizedName>
            <Required>no</Required>
            <Type>i32</Type>
         </Parameter>
         <Parameter name="Timed Out">
            <Direction>out</Direction>
            <LocalizedName localize="true">Timed Out</LocalizedName>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Wait on Rising Edge">
      <LocalizedName localize="true">Wait On Rising Edge</LocalizedName>
      <ParameterList>
         <Parameter name="Timeout">
            <Direction>in</Direction>
            <LocalizedName localize="true">Timeout</LocalizedName>
            <Required>no</Required>
            <Type>i32</Type>
         </Parameter>
         <Parameter name="Timed Out">
            <Direction>out</Direction>
            <LocalizedName localize="true">Timed Out</LocalizedName>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O      � �               displayFilter �                   tdData �               IOInterface �    @0����name      �<Interface>
<MethodSet>
   <Method name="Read">
      <LocalizedName localize="true">Read</LocalizedName>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <LocalizedName localize="true">Write</LocalizedName>
      <ParameterList>
         <Parameter name="Value">
            <LocalizedName localize="true">Value</LocalizedName>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O        � �               displayFilter �                   tdData �               IOInterface �    @0����name      �<Interface>
<MethodSet>
   <Method name="Read">
      <LocalizedName localize="true">Read</LocalizedName>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <LocalizedName localize="true">Write</LocalizedName>
      <ParameterList>
         <Parameter name="Value">
            <LocalizedName localize="true">Value</LocalizedName>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O        � �               displayFilter �                   tdData �               IOInterface �    @0����name      �<Interface>
<MethodSet>
   <Method name="Read">
      <LocalizedName localize="true">Read</LocalizedName>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <LocalizedName localize="true">Write</LocalizedName>
      <ParameterList>
         <Parameter name="Value">
            <LocalizedName localize="true">Value</LocalizedName>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O        �  �x�͙�oe�?ϵݮ]Ǯ�o�ۺ�:`P� 	_v ��n&�ڍʶ�v�2P�
�`�	H��A����&�~1L*�Ԅ@`��sw���ۨM�l˞<�>�/���~=m� f��̤F�G �<��V,�0t��a� S�D�w�F�5J�`����J�4��K�Q� 9G�`���<Ǝ�,y�3l/���j{ڤ���\��v�%�3| ��@m.2
�+5��:O��g��fm�]�`�pa���G���쒪"'��%����ק��r9��hCN�^�j�b.<7�F�6����q��ѳR�Q��3\�AS�;ݲd��	ke���a��1jw@��ld=m��{�
w�׀ ���؋}�����AU�j�^���g5a�8wP���W�(E����f�f,�f��r3��i�n\xJ78�`�lwȿoCGs��:�5��kW���t$wi6��* ����H������|�2G�t%��`#1;V�cU�jU��j�{��=jh�*謦d��ˢ���-�G���]i4|)�{4a/Faۘļㄽ4�®D��$�I�C��#��+�\���*�9'�
��J�3���Mv�+&����8;l�s1a	�	{D��}Q�F�`Hv�lшM��(��U$`>�`�6�ЗR�;�l�8�@t�6_{ x4����|�&�T.R�C�@.Y��K�@0|�B�b#���3��ו��Jp5�أE}_���G��%ш��5o�!�*��b�)��Z[�&�VEt����N��X���X�>���:y�r�R��ҀNg�5Z�Xv�`�"���;�	_*-aʢ�v�eJ�)�J����f�ra�D�c@�È�ӡP�Ӄ4j3|�pH?�4��=	ߣܜ���x�����۫�H��-���\���~���'&�u�s�2F�W����g���n~Sٳt��{w:��ɚ�;����5l���J��@}�@W��g������u�@�����_-m�-�Z=Kڏ)�R�v~�o�%�1�r#�b*���	��iQ�?JF�/k�[�8�eN='xVâN�l�8�s���ON�7kN�B���ǩ9�O'����������k��������i�T�H��aq$��ҒҘ)�eS�OH����X����Y�3h֎4�&	4볇�&[h�4м8���1�
�M)��5h�hЌ����Dh������)R��i4LEMm���匚�@M�$�Ɲ=5�RӘ��]@�YG�)W��NA�E�f���l>RS�H�޴��ʎW25�2�Ɠ3j��5͓�o������%5�	��@GM^��y55V��v��z��X"5����fHM�JM�/��Lf�P��s�Lh��$�te�Lw��N��k�L����\1�z
f�D_����J-~��'�]'V��DJl
P��bcS�d��������5'�� oz�FIȖ���EP�����c�6�ZY����]|�sIe���P��%{���Ή9�� ����7S�ڊK�Ri�I�9Y��R��e�4�/���y[aߠ�f�Wd���̳��ꚏ���V�iI�6��}T����R�|�n��i|(��I��p���)�}}���r����e��\��eM߻�{�D�YN���%���[����!�E^��]l�)���!��hG�e�O�Xi1pK������l��Z����5J� ��K���c���,�;p������3�;ZĀ        ~    BDHP	Clock.ctl          b   rx�c``��`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-�����z�\�8Se�<� b            1      NI.LV.All.SourceOnly �     !                    �   (         
                                x��X�n�@�����	q�ʕ�KA��Z��"�u�T��x�,8�a�.JO�;����� 7ne�q'M�&'��ݝ�om����ؽT?Xڂ�+ � ��
�u�h�DQwj�l��"sZo�����f�lr���Z#��C��e*�=,��̗�7�Q�~��]�vY���Q�:.��Bl���^�{���G{)|�X=&��$�j
��e��:$�5�!�뼜:���+~����JR�EʂĚ$E��lޗ<��}�݉y豌����62Z9ʐ�
�($�S#Jj�~c\�oH��Ԙ��`�u��I����i������i��2F7��L�H�uɡ���-��q� w��ˎPC%���A|��B���B��Q�P��A9�(3��hm�ԓ�W�Ͼbw-ƃ��a�e�jg8�:�Q87��{R�z�����'z�j�G��p�"�9u�Ȃ�y�dUƱ�2e�W��h�IJx�޴� U&���*K��l����&M�W�ڙ��&�Lt9�.�wAf�V:�Ӆ��<)ڐ˵��Jodغ�i�a�ո��~�H�<��=�f�ό����W����g>0��)�����cI�Y��Ir���q_��I��n��`M���W���L�^<���#�h�3�P��V'	��N��S]�A)�aa5��0Z�g��k�T�j��#?��^^�f ���rh
Ori�ꕨ���]�U�A~���� V�ꋚ��u�iEqOQ-�2����j�~�j��|�i�N�"��)����ZY���.�B�>�V��-�~    e       H      � �   Q      � �   Z      � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW  -l  F      -L               4  <   LVSR       RTSG      vers      (CONP      <LIvi      PICON      dicl8      xCPC2      �LIfp      �STR      �FPHb      ,FPSE      @LIbd      TBDHb      hBDSE      |VITS      �DTHP      �MUID      �HIST      �VCTP      �FTAB      �                        ����       �       ����       �        ����       �        ����       �        ����       �        ����      \        ����      `        ����      h       ����      �       ����      �       ����      	�       ����      �       ����      d       ����               ����      !�        ����      (�        ����      (�        ����      (�        ����      )@        ����      )H        ����      )�        ����      )�        ����      )�        ����      )�       �����      ,�    	Clock.ctl