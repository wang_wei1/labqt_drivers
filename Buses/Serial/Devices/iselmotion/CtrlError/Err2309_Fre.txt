;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_IMC4CTRL

0-2309-0: Aucune erreur!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2309-1: 1: Le nombre transf�r� est erron�!                                                 
3-2309-2: 2: Un capteur de limite d'axe est activ�, une mise en r�f�rence est requise!           
3-2309-3: 3: Sp�cification d'axe invalide!                                                          
3-2309-4: 4: Pas d'axe sp�cifi�, un reset est requis!                                                
3-2309-5: 5: Syntaxe erreur: commande erron�e ou inconnue!                                         
3-2309-6: 6: D�passement m�moire! Le montant du transfert des donn�es exc�de la taille de la capacit� m�moire.    
3-2309-7: 7: Nombre de param�tres invalide!                                                        
3-2309-8: 8: La commande CNC transf�r�e est incorrecte!                                            
3-2309-9: 9: Erreur machine commune: Machine hors puissance, arr�t d'urgence enclench�, capot ouvert, ...)!
3-2309-10: A: Code erreur non utilis� par la carte IMC4!
3-2309-11: B: Code erreur non utilis� par la carte IMC4!
3-2309-12: C: Code erreur non utilis� par la carte IMC4!
3-2309-13: D: Vitesse invalide. M�me pour un mouvement d'axe nul une vitesse doit �tre sp�cifi�e.
3-2309-14: E: Code erreur non utilis� par la carte IMC4!
3-2309-15: F: Stop utilisateur activ�!
3-2309-16: G: Le champ de donn�e est invalide!
3-2309-17: H: Le capot est ouvert, la commande ne peut �tre ex�cut�e!
3-2309-18: =: Code erreur non utilis� par la carte IMC4!

;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors

;//////////////////////////////////////////////////////////////
;End Of File
