;**************************************************************
;Error messages for Control IMS6
;Do not modify this text file!
;(c) isel Automation KG 2000. All rights reserved.
;**************************************************************
;Facility: FAC_IMS6CTRL

0-2314-0: Kein Fehler!

;//////////////////////////////////////////////////////////////

;Warnings
;//////////////////////////////////////////////////////////////

2-2314-385: Bewegung mit resultierender Länge 0!
2-2314-1409: Ein nicht unterstützter Befehl wurde ignoriert!
2-2314-1410: Ein nicht bekannter Befehl wurde ignoriert!
2-2314-1411: Ein nicht bekannter Initialisierungsbefehl	wurde ignoriert!
2-2314-1412: Ein unbekannter oder überflüssiger Parameter wurde ignoriert!
2-2314-1413: Ein Token innerhalb eines Befehls konnte nicht dekodiert werden!

;Errors
;//////////////////////////////////////////////////////////////

3-2314-257: Befehl kann nicht ausgeführt werden, da min 1 Achse in Bewegung ist!
3-2314-258: Befehl kann nicht ausgeführt werden, da TeachInModus!
3-2314-259: Befehl kann nicht ausgeführt werden, da Bahnbearbeitung noch aktiv!
3-2314-260: Befehl kann nicht ausgeführt werden, da ungültige Geschwindigkeit!
3-2314-261: Befehl kann nicht ausgeführt werden, da Zugriff auf nicht aktive Achse!
3-2314-262: Befehl kann nicht ausgeführt werden, da Stopmodus aktiv!
3-2314-263: Bewegung wurde wegen Übertemperatur unterbrochen!
3-2314-264: Zugriff auf falsche E/A Portnummer!
3-2314-265: Zugriff auf nicht aktiven Port!
3-2314-266: Zugriff auf falsche E/A Bitnummer!
3-2314-267: Zugriff auf nicht aktiven Bit!
3-2314-268: Referenzfahrt kann nicht ausgeführt werden da zugehöriger RefSchalter nicht aktiv!
3-2314-269: Es wurde keine Referenzfahrt durchgeführt!
3-2314-270: Es ist ein Hardwareendschalterfehler aufgetreten!
3-2314-271: Während einer Bewegung ist ein Hardwareenschalterfehler aufgetreten!
3-2314-272: Befehl kann nicht ausgeführt werden, da vorher der Break-Befehl aufgerufen wurde!
3-2314-273: Befehl kann nicht ausgeführt werden, da der Handmodus aktiv ist!

3-2314-513: Protokollfehler, Steuerzeichenfolge wurde nicht eingehalten!
3-2314-514: Protokollfehler, CRC - Fehler ist aufgetreten!
3-2314-515: Protokollfehler, Fehler in der Länge des Datenblocks!
3-2314-516: Protokollfehler, unerwartetes Steuerzeichen SOH!
3-2314-517: Protokollfehler, unerwartetes Steuerzeichen STX!
3-2314-518: Protokollfehler, unerwartetes Steuerzeichen ETX!
3-2314-519: Protokollfehler, unerwartetes Steuerzeichen EOT!
3-2314-520: Der Befehlspuffer ist übergelaufen!
3-2314-521: Der Bahnpuffer ist übergelaufen!
3-2314-522: Es ist ein Protokollfehler aufgetreten!

3-2314-769: Der Notaustaster wurde aktiviert!
3-2314-784: Die Motorspannung der Achse X ist ausgefallen!
3-2314-785: Die Motorspannung der Achse Y ist ausgefallen!
3-2314-786: Die Motorspannung der Achse Z ist ausgefallen!
3-2314-787: Die Motorspannung der Achse A ist ausgefallen!
3-2314-788: Die Motorspannung der Achse B ist ausgefallen!
3-2314-789: Die Motorspannung der Achse C ist ausgefallen!
3-2314-790: Übertemperatur der Endstufe Achse X!
3-2314-791: Übertemperatur der Endstufe Achse Y!
3-2314-792: Übertemperatur der Endstufe Achse Z!
3-2314-793: Übertemperatur der Endstufe Achse A!
3-2314-794: Übertemperatur der Endstufe Achse B!
3-2314-795: Übertemperatur der Endstufe Achse C!

3-2314-1281: Fehlende Parameter innerhalb eines Befehls!
3-2314-1282: Ein Parameter besitzt einen ungültigen Parameter!
3-2314-1536: Die aufgerufene Funktion ist ungültig!


;//////////////////////////////////////////////////////////////
;End Of File