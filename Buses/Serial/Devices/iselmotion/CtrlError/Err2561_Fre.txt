;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_SPINDLE

0-2561-0: Aucune erreur!

;//////////////////////////////////////////////////////////////
;Specific section

;Errors
3-2561-1: Vitesse d'axe invalide! La vitesse indiqu�e ne peut pas �tre plac�e par cette commande d'axe.
3-2561-2: Externes Fehlersignal (z.B. Spannzange nicht geschlossen oder kein Luftdruck) am Umrichter aktiv! Die Spindel darf nicht eingeschaltet werden.
3-2561-3: Ung�ltige Minimal- oder Maximalgeschwindigkeit! Bei der Einstellung der Spindelparameter sind die Parameter f�r die Spindelminimal- bzw. die Spindelmaximalgeschwindigkeit fehlerhaft (evtl. vertauscht?).
3-2561-17: Zugriff auf die IO-Verwaltung der Steuerung nicht m�glich! Der IO-Bereich der Steuerung wurde bisher noch nicht initialisiert odr es liegt ein schwerwiegender Fehler vor, evtl. liegt ein Versionskonflikt vor?
3-2561-33: Der Eingang des externen St�rungssignals 1 ist aktiv. Die Spindel kann nicht gestartet werden.
3-2561-34: Der Eingang des externen St�rungssignals 2 ist aktiv. Die Spindel kann nicht gestartet werden.
3-2561-35: Externe St�rung: Unzureichender Luftdruck! Die Spindel kann nicht gestartet werden.
3-2561-36: Externe St�rung: Die Spannzange ist ge�ffnet! Die Spindel kann nicht gestartet werden.
3-2561-37: Externe St�rung: Der Werkzeughalter ist nicht richtig in die Spannzange eingezogen! Die Spindel kann nicht gestartet werden.


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings
2-2561-4101: Le module d'axe est d�sactiv�! La fonction demand�e ne peut pas �tre ex�cut�e.

;Errors
3-2561-4097: DLL non trouver!
3-2561-4098: Impossible de trouver les donn�es essentielles d'initialisation de ce module d'axe!
3-2561-4099: Conflit de version ! Le module d'axe ne fournit pas la fonction demand�e.
3-2561-4100: Index de module invalide! Le module demand� d'axe n'existe pas.
3-2561-4101: Le module d'axe choisi est d�sativ�!
3-2561-4102: Le module d'axe choisi n'est pas encore initialis�!
3-2561-4103: Le module d'axe est occup� par un autre appel de fonction!
3-2561-4353: Fichier non trouv�!
3-2561-4354: Impossible d'ouvrir le fichier!
3-2561-4355: Impossible de lire dans le fichier!
3-2561-4356: Impossible d'�crire dans le fichier!
3-2561-4369: Impossible de trouver le fichier d'initialisation du module d'axe!
3-2561-4370: Impossible d'ouvrir le fichier d'initialisation du module d'axe!
3-2561-4371: Impossible de lire le fichier d'initialisation du module d'axe!
3-2561-4372: Impossible d'�crire dans le fichier d'initialisation du module d'axe!
3-2561-4373: Version incorrect du fichier d'initialisation du module d'axe!
3-2561-4386: Impossible d'ouvrir un fichier temporaire!
3-2561-4387: Impossible de lire dans un fichier temporaire!  
3-2561-4388: Impossible d'�crire dans un fichier temporaire!
3-2561-4401: Nom de fichier invalide!
3-2561-4609: Coupure d'utilisateur ! La derni�re action ex�cut�e a �t� avort�e � la demande de l'utilisateur.
3-2561-4610: Erreur mat�riel!
3-2561-4611: �cheque de puissance!
3-2561-4625: L'axe est bloqu� par le module de circuit de s�curit�!
3-2561-4641: Could not start a worker thread!
3-2561-4642: Une valeur d'index invalide a �t� utilis�e!
3-2561-4643: Des donn�es incorrectes ou invalide se sont produites!
3-2561-4644: Une position invalide a �t� donn� au control resp. Une erreur de position s'est produite!
3-2561-4645: Une variable de processus n'a pas pu �tre cr��e!
3-2561-4646: Function inconnue : L'une des fonctions demand�es est inconnue dans la DLL o� la version utilis�e est trop ancienne !
3-2561-4647: Pointer nul : Un pointeur invalide a �t� donn� !
3-2561-4648: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2561-4649: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2561-4650: Num�ro de port invalide : La valeur est soit n�gative, soit trop grande !
3-2561-4651: Nombre de bit invalide : La valeur est soit n�gative, soit sup�rieur � 7!
3-2561-4865: Erreur RS232: Impossible d'�tablir la connection s�rie avecle convertisseur de fr�quence!
3-2561-4866: Erreur RS232: Impossible d'ouvrir le port s�rie.
3-2561-4867: Erreur RS232: Impossible de d�finit les param�tres de communication.
3-2561-4868: Erreur RS232: Impossible d'activer le port s�rie!
3-2561-4869: Erreur RS232: Il manque la connection s�rie pour le convertisseur de fr�quence!
3-2561-4870: Erreur RS232: Une erreur s'est produite pendant l'envoie des donn�es.
3-2561-4871: Erreur RS232: Une erreur s'est produite pendant la r�ception des donn�es.
3-2561-4872: Erreur RS232: Timeout error during RS232 communication!
3-2561-5121: La porte de la machine est ouverte!
3-2561-5122: La porte de la machine est fermer!
3-2561-5137: L'axe est encore aliment�!
3-2561-5138: L'axe n'est plus aliment�!
3-2561-5139: Impossible de d�finir le sens de rotation d�sir� !
3-2561-5153: Les axes de la machines ne sont pas en position d'origine!
3-2561-5154: Les axes de la machines sont en position d'origine!
3-2561-5155: Les axes de la machines ne sont pas en position d'arret!
3-2561-5156: Les axes de la machines sont en position d'arret!
3-2561-65535: Erreur inconnue!

;//////////////////////////////////////////////////////////////
;End Of File
