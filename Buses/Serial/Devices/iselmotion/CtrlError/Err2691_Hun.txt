;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_IOADM

0-2691-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
;Specific section

3-2691-1: Ung�ltige logische Portnummer! Die �bergebene Portnummer ist im Bereich der IO-Verwaltung ung�ltig.
3-2691-2: Ung�ltige Bitnummer! Die �bergebene Bitnummer ist im Bereich der IO-Verwaltung ung�ltig.
3-2691-3: Port ist deaktiviert! Das gew�hlte Port wurde in der IO-Verwaltung deaktiviert.

3-2691-4: Ung�ltige logische Portnummer eines System-Ausgangs! Die �bergebene Portnummer ist im Bereich der IO-Verwaltung ung�ltig.
3-2691-5: Ung�ltige Bitnummer eines System-Ausgangs! Die �bergebene Bitnummer ist im Bereich der IO-Verwaltung ung�ltig.
3-2691-6: Der angeforderte System-Ausgang ist disabled! Das gew�hlte System-Port wurde in der IO-Verwaltung deaktiviert.

3-2691-17: Der angeforderte Eingang ist deaktiviert! Der gew�hlte User-Eingangs wurde in der IO-Verwaltung deaktiviert.
3-2691-18: Der angeforderte Ausgang ist deaktiviert! Das gew�hlte User-Ausgang wurde in der IO-Verwaltung deaktiviert.
3-2691-19: Der angeforderte System-Eingang ist deaktiviert! Der gew�hlte System-Eingangs wurde in der erweiterten IO-Verwaltung deaktiviert.
3-2691-20: Der angeforderte System-Ausgang ist deaktiviert! Das gew�hlte System-Ausgang wurde in der erweiterten IO-Verwaltung deaktiviert.

3-2691-33: Fehler beim Zugriff auf einen Peripherie-Ausgabekanal! Der Zugriff auf ein Peripherieger�t konnte nicht korrekt ausgef�hrt werden. �berpr�fen Sie die Konfiguration der Peripherieger�te.
3-2691-34: Fehler beim Initialisieren eines Peripherie-Ausgabekanals! Die Initialisierung eines Peripherieger�ts oder mehrerer Peripherieger�te konnte nicht korrekt ausgef�hrt werden. �berpr�fen Sie die Konfiguration der Peripherieger�te.

3-2691-49: Ung�ltige Nummer eines Analogkanals! Der analoge Ein- oder Ausgabekanal mit der �bergebenen Kanalnummer existiert nicht.
3-2691-50: Der angeforderte Analogkanal ist deaktiviert! Der Analogeingang oder -ausgang mit der �bergebenen Kanalnummer existiert nicht bzw. wurde nicht in den erweiterten IO-Einstellungen eingetragen.

;//////////////////////////////////////////////////////////////
;Common Section

;Warnings
2-2691-4101: Das gew�hlte IO-Modul ist deaktiviert, die angeforderte Funktion konnte nicht ausgef�hrt werden!

;Errors
3-2691-4097: Die zugewiesene IO-DLL wurde nicht gefunden!
3-2691-4098: Notwendige Initialisierungsdaten wurden nicht gefunden!
3-2691-4099: Falsche Version einer verbundenen IO-DLL!
3-2691-4100: Ung�ltiger Modul-Index!
3-2691-4101: Das gew�hlte IO-Modul ist deaktiviert, die angeforderte Funktion ist gesperrt!
3-2691-4102: Das gew�hlte IO-Modul wurde bisher nicht initialisiert!
3-2691-4103: Das IO-Modul ist bereits mit einer weiteren Anforderung besch�ftigt!
3-2691-4369: Die Initialisierungsdatei NCCTRL.INI wurde nicht gefunden!
3-2691-4370: Die Initialisierungsdatei NCCTRL.INI kann nicht ge�ffnet werden!
3-2691-4371: Aus der Initialisierungsdatei NCCTRL.INI kann nicht gelesen werden!
3-2691-4372: In die Initialisierungsdatei NCCTRL.INI kann nicht geschrieben werden!
3-2691-4373: Falsche Version der Initialisierungsdatei NCCTRL.INI!
3-2691-4386: Eine tempor�re Datei kann nicht ge�ffnet oder erstellt werden!
3-2691-4387: Aus einer tempor�ren Datei kann nicht gelesen werden!
3-2691-4388: In eine tempor�re Datei kann nicht geschrieben werden!
3-2691-4401: Ung�ltiger Dateiname!
3-2691-4609: Abbruch durch den Anwender!
3-2691-4641: Ein Arbeits-Thread konnte nicht gestartet werden!
3-2691-4642: Ein ung�ltiger Index wurde benutzt!
3-2691-4643: Es liegen ung�ltige Daten vor!
3-2691-4644: Eine ung�ltige Position wurde �bergeben bzw. ein ung�ltiger Positionswert liegt vor!
3-2691-4645: Eine ben�tigte Prozessvariable konnte nicht erzeugt werden!
3-2691-4646: Unbekannte Funktion: Entweder ist die Funktion in der verwendeten Modul-DLL unbekannt oder es liegt eine veraltete Version der DLL vor!
3-2691-4647: NULL Pointer: Es liegt ein ung�ltiger Pointer vor!
3-2691-4648: Ung�ltige Geschwindigkeit, der �bergebene Wert ist entweder negativ oder zu gro�!
3-2691-4649: Ung�ltige Drehzahl, der �bergebene Wert ist entweder negativ oder zu gro�!
3-2691-4650: Ung�ltige Portnummer: Die �bergebene Portnummer ist kleiner als 1 oder zu gro�!
3-2691-4651: Ung�ltige Bitnummer: Die �bergebene Bitnummer ist kleiner als 1 oder gr��er als 8!
3-2691-65535: Unbekannter Fehler!

;//////////////////////////////////////////////////////////////
;End Of File
