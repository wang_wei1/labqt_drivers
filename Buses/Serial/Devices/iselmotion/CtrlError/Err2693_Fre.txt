;**************************************************************
;Error messages for MctlAdm.DLL / Frame administration section
;Do not modify this text file!
;(c) isel Automation KG 2000. All rights reserved.
;**************************************************************
;Facility: FAC_FRAMEADM

0-2693-0: Aucune erreur!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2693-1: The given frame position pointer is invalid (NULL pointer assignment)!
3-2693-17: Could not find the requested frame position. Perhaps the frame position is not yet declared?
3-2693-33: Could not find a required entry in the list of frame positions. Eventually the required frame position entry was not created?
3-2693-34: The passed frame file name is invalid!
3-2693-35: Could not open the frame file!
3-2693-36: Could not write into the frame file!
3-2693-37: Could not read frame file!


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors
3-2693-4097: Impossible de trouver la DLL d'administration de commande de mouvement!
3-2693-4098: Impossible de trouver les donn�es essentielles d'initialisation!
3-2693-4099: Version incorrect de la DLL d'administration de commande de mouvement ! La fonction demand�e ne peut �tre executer!
3-2693-4100: Index de module invalide!
3-2693-4103: La DLL d'administration de commande de mouvement est occup� par un autre appel de fonction!
3-2693-4369: Impossible de trouver le fichier d'initialisation MCTLADM.INI!
3-2693-4370: Impossible d'ouvrir le fichier d'initialisation MCTLADM.INI!
3-2693-4371: Impossible de lire le fichier d'initialisation MCTLADM.INI!
3-2693-4372: Impossible d'�crire dans le fichier d'initialisation MCTLADM.INI!
3-2693-4373: Version incorrect du fichier d'initialisation MCTLADM.INI!
3-2693-4386: Impossible de cr�er/d'ouvrir un fichier temporaire!
3-2693-4387: Impossible de lire dans un fichier temporaire!
3-2693-4388: Impossible d'�crire dans un fichier temporaire!
3-2693-4401: Nom de fichier invalide!
3-2693-4609: Coupure d'utilisateur ! La derni�re action ex�cut�e a �t� avort�e � la demande de l'utilisateur.
3-2693-4641: Could not start a worker thread!
3-2693-4642: Une valeur d'index invalide a �t� utilis�!
3-2693-4643: Des donn�es incorrectes ou invalide se sont produites!
3-2693-4644: Une position invalide a �t� donn� au control resp. Une erreur de position s'est produite!
3-2693-4645: Une variable de processus n'a pas pu �tre cr��e!
3-2693-4646: Function inconnue : L'une des fonctions demand�es est inconnue dans la DLL o� la version utilis�e est trop ancienne !
3-2693-4647: Pointer nul : Un pointeur invalide a �t� donn� !
3-2693-4648: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2693-4649: Vitesse invalide : La valeur est soit n�gative, soit trop grande !
3-2693-4650: Num�ro de port invalide : La valeur est soit n�gative, soit trop grande !
3-2693-4651: Nombre de bit invalide : La valeur est soit n�gative, soit sup�rieur � 7!
3-2693-65535: Erreur inconnue!

;//////////////////////////////////////////////////////////////
;End Of File
