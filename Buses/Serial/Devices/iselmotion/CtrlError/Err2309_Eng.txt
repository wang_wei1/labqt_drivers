;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_IMC4CTRL

0-2309-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2309-1: 1: The transferred number is erroneous!                                                 
3-2309-2: 2: At least one hardware limit switch is active, a reference run is required!           
3-2309-3: 3: Invalid axis specification!                                                          
3-2309-4: 4: No axes specified, Reset is required!                                                
3-2309-5: 5: Syntax error: Unknown or erroneous command!                                          
3-2309-6: 6: Memory overflow! Amount of transferred data exceeds the size of available memory.    
3-2309-7: 7: Invalid number of parameters!                                                        
3-2309-8: 8: The transferred CNC command is incorrect!                                            
3-2309-9: 9: Common machine error: Power fail, Emergency Switch pressed, cover is open, ...)!
3-2309-10: A: Error code not used by IMC4 control!
3-2309-11: B: Error code not used by IMC4 control!
3-2309-12: C: Error code not used by IMC4 control!
3-2309-13: D: Invalid velocity. Even axes with movement length 0 expect a valid velocity specification.
3-2309-14: E: Error code not used by IMC4 control!
3-2309-15: F: User stop active!
3-2309-16: G: The given data field is invalid!
3-2309-17: H: The cover is open, could not execute the given command!
3-2309-18: =: Error code not used by IMC4 control!

;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors

;//////////////////////////////////////////////////////////////
;End Of File
