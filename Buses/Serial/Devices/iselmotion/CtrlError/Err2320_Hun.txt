;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 2001. All rights reserved.
;**************************************************************
;Facility: FAC_PARPORT_CAN_WDM

0-2320-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2320-1: ISA- oder EISA-Bus nicht vorhanden!
3-2320-2: Nicht genug RAM-Speicher!
3-2320-3: WDM_Treiber nicht frei!
3-2320-4: Hardwarekonflikt!
3-2320-5: Basistreiber des Parallelports nicht vorhanden!
3-2320-6: Basistreiber des Parallelports nicht frei!
3-2320-7: Verbinden mit dem Parallelportinterrupt nicht m�glich!
3-2320-8: Hardware vom Can-Dongle defekt!
3-2320-9: Trennen vom Parallelportinterrupt nicht m�glich!
3-2320-10: Can-Dongle noch nicht initialisiert!
3-2320-11: Falsche Initialisierungsparameter f�r Can-Dongle!
3-2320-12: Can-Dongle bereits initialisiert!
3-2320-13: Falsche Datei-Handle-Nummer!
3-2320-14: Falsche Parameter f�r gebufferte Interpolation!
3-2320-15: Can-Kontroller vom Bus getrennt!
3-2320-16: Messung der Latenzzeit nicht aktiv!


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors


;//////////////////////////////////////////////////////////////
;End Of File
