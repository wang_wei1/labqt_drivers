;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_AXCTL

0-2422-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
;Specific section


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings
2-2422-4101: Das Modul AxCtl.DLL f�r die Ansteuerung unabh�ngiger Achsen ist deaktiviert, die angeforderte Funktion konnte nicht ausgef�hrt werden!

;Errors
3-2422-1: Allgemeiner, nicht n�her spezifizierter Fehler! Versuchen Sie, die gew�nschte Operation nach einem Neustart des Anwenderprogramms und erneuter Initialisierung nochmals auszuf�hren.
3-2422-2: Fehler bei der Operationsausf�hrung! Im Allgemeinen war eine SDO bzw. PDO-Kommunikation nicht m�glich.
3-2422-3: Timeout-Fehler beim Warten auf ein Ereignis (Funktion axctl_WaitForEvent()).
3-2422-4: Mindestens eine Achse befindet sich in einem fehlerhaften Zustand. Im Allgemeinen ist ein Reset notwendig oder ein Endschalter mu� freigefahren werden oder die Versorgungsspannung fehlt.
3-2422-5: Referenzfahrt-Fehler! Es wurde noch keine Referenzfahrt f�r die angeforderte Achse durchgef�hrt.
3-2422-6: Ung�ltiger Handle bzw. Achsenindex. Die angeforderte Achse existiert nicht oder wird nicht verwendet.
3-2422-7: DLL nicht initialisiert! Es wurde noch keine erfolgreiche Initialisierung mit der Funktion "Handlingachsen initialisieren" durchgef�hrt.
3-2422-8: DLL bereits initialisiert! Die Funktion "Handlingachsen initialisieren" wurde bereits durchgef�hrt.
3-2422-9: Fehler beim Lesen der Achsenkonfiguration aus der Initialisierungsdatei f�r die Handlingachsen.
3-2422-10: Falscher Operationsmodus! Bei Drehzahlreglung ist keine Positionierung m�glich oder umgekehrt. (Betrifft die Funktionen axctl_MoveAbs(), axctl_MoveRel()).
3-2422-11: Fehler beim �ffnen der Initialisierungdatei!
3-2422-12: Fehler beim �ffnen der seriellen Schnittstelle!
3-2422-13: Ung�ltige Parameter! (Strukturgr��e unterschiedlich). Dies deutet auf die Verwendung einer veralteten Version der Bibliothek AxCtl.DLL hin.

3-2422-201: CanAPI.DLL nicht gefunden bzw. nicht initialisiert. F�r die Verwendung des Moduls f�r den Betrieb von Handling-Achsen (AxCtl.DLL) mu� eine CAN-Steuerung verwendet werden.

3-2422-4097: Das Modul AxCtl.DLL f�r die Ansteuerung unabh�ngiger Achsen wurde nicht gefunden! Diese Bibliothek sollte sich im gleichen Verzeichnis wie die CAN-Steuerungs-DLL CanAPI.DLL, z.B. \CNCWorkbench\Control\CAN, befinden.
3-2422-4098: Notwendige Initialisierungsdaten wurden nicht gefunden!
3-2422-4099: Falsche Version des Moduls AxCtl.DLL! Es wurde versucht, eine Funktion zur Ansteuerung einer Handlingachse aufzurufen, f�r die ein neuerer Release-Stand erforderlich ist.
3-2422-4101: Das Modul AxCtl.DLL f�r die Ansteuerung unabh�ngiger Achsen ist deaktiviert, die angeforderte Funktion ist gesperrt!
3-2422-4102: Das Modul AxCtl.DLL f�r die Ansteuerung unabh�ngiger Achsen wurde bisher nicht initialisiert!
3-2422-4103: Das Modul AxCtl.DLL f�r die Ansteuerung unabh�ngiger Achsen ist bereits mit einer weiteren Anforderung besch�ftigt!
3-2422-4386: Eine tempor�re Datei kann nicht ge�ffnet oder erstellt werden!
3-2422-4387: Aus einer tempor�ren Datei kann nicht gelesen werden!
3-2422-4388: In eine tempor�re Datei kann nicht geschrieben werden!
3-2422-4401: Ung�ltiger Dateiname!
3-2422-4609: Abbruch durch den Anwender!
3-2422-4641: Ein Arbeits-Thread konnte nicht gestartet werden!
3-2422-4642: Ein ung�ltiger Index wurde benutzt!
3-2422-4643: Es liegen ung�ltige Daten vor!
3-2422-4644: Eine ung�ltige Position wurde �bergeben bzw. ein ung�ltiger Positionswert liegt vor!
3-2422-4645: Eine ben�tigte Prozessvariable konnte nicht erzeugt werden!
3-2422-4646: Unbekannte Funktion: Entweder ist die Funktion in der verwendeten Modul-DLL AxCtl.DLL unbekannt oder es liegt eine veraltete Version der DLL vor!
3-2422-4647: NULL Pointer: Es liegt ein ung�ltiger Pointer vor!
3-2422-4648: Ung�ltige Geschwindigkeit, der �bergebene Wert ist entweder zu gro� oder er konnte nicht ermittelt werden!
3-2422-4649: Ung�ltige Drehzahl, der �bergebene Wert ist entweder negativ oder zu gro�!
3-2422-65535: Unbekannter Fehler!

;//////////////////////////////////////////////////////////////
;End Of File
