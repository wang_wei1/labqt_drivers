;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_COMPILER

;//////////////////////////////////////////////////////////////
;Specific section

0-2817-0: No error!

3-2817-1: Could not find source file %s!
3-2817-2: Could not open source file %s!
3-2817-3: Could not read from source file %s!

3-2817-17: Could not open target file %s!
3-2817-18: Could not write to target file %s!

3-2817-33: Could not open log file %s!
3-2817-34: Could not write to log file %s!
3-2817-35: Could not open label file %s!
3-2817-36: Could not write to label file %s!
3-2817-37: Could not open error log file %s!
3-2817-38: Could not write to error log file %s!
3-2817-39: Could not open offset file %s!

3-2817-49: Compiler error!
3-2817-50: Invalid option: %s!
3-2817-51: Missing argument or argument incomplete: %s!

3-2817-65: Invalid source file name: %s!
3-2817-66: Invalid target file name: %s!
3-2817-67: Invalid source file extension: %s!

3-2817-80: One/several syntactical error occured during compilation!


;//////////////////////////////////////////////////////////////
;Common Section

3-2817-4097: DLL not found!
3-2817-4098: Could not find essential compiler initialization data!
3-2817-4099: Version conflict! The installed compiler module does not provide the requested function!
3-2817-4100: Invalid module index!
3-2817-4101: The compiler module is disabled!
3-2817-4102: The compiler module is not yet initialized!
3-2817-4103: The compiler module is busy with another function call!
3-2817-4353: File not found!                                                  
3-2817-4354: Could not open file!
3-2817-4355: Could not read from file!
3-2817-4356: Could not write to file!
3-2817-4369: Could not find the compiler initialization file!
3-2817-4370: Could not open the compiler initialization file!
3-2817-4371: Could not read from compiler initialization file!
3-2817-4372: Could not write to the compiler initialization file!
3-2817-4373: Incorrect version of the compiler initialization file!
3-2817-4386: A temporary file could not be opened!
3-2817-4387: Could not read from a temporary file!
3-2817-4388: Could not write to a temporary file!
3-2817-4401: Invalid file name!
3-2817-4609: User break! The last executed action was aborted by user request.
3-2817-4610: Hardware error!
3-2817-4611: Power fail!
3-2817-4612: PC speed is too low!
3-2817-4625: Operation is locked by the security circuit module!
3-2817-4641: Could not start a worker thread!
3-2817-4642: An invalid index value was used!
3-2817-4643: Erroneous or invalid data occured!
3-2817-4644: An invalid position was given to the control resp. erroneous positions occured!
3-2817-4645: A process variable could not be created!
3-2817-4646: Unknown function: Either the requested function is unknown within the module DLL or the version of the used DLL is too old!
3-2817-4647: NULL Pointer: A invalid pointer was given!
3-2817-4648: Invalid velocity: The given value is either negative or too big!
3-2817-4649: Invalid speed: The given value is either negative or too big!
3-2817-4650: Invalid port number: The given value is either negative or too big!
3-2817-4651: Invalid bit number: The given value is either negative or greater than 7!
3-2817-4865: RS232 error: Could not establish the serial connection!
3-2817-4866: RS232 error: Could not open the serial port!
3-2817-4867: RS232 error: Could not set the parameters for serial transmission!
3-2817-4868: RS232 error: Could not release the serial port!
3-2817-4869: RS232 error: Missing serial connection!
3-2817-4870: RS232 error: Error during sending of data!
3-2817-4871: RS232 error: Error during receiving of data!
3-2817-4872: RS232 error: Timeout error during RS232 communication!
3-2817-5121: The cover of the machine is open!
3-2817-5122: The cover of the machine is closed!
3-2817-5137: The spindle is still switched on!
3-2817-5138: The spindle is switched off!
3-2817-5139: Could not set the requested turn direction!
3-2817-5153: The machines axes are not at their home position!
3-2817-5154: The machines axes are at their home position!
3-2817-5155: The machines axes are not at their park position!
3-2817-5156: The machines axes are at their park position!
3-2817-65535: Unknown error!

;//////////////////////////////////////////////////////////////
;End Of File


