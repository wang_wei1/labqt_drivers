;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 2001. All rights reserved.
;**************************************************************
;Facility: FAC_CANSET

0-2325-0: No error!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2325-1: Could not find given variable in the initialization file!
3-2325-2: Could not open the initialization file (does not exist, no rights, ...)!
3-2325-3: Error while reading the initialization file (Syntax error, variables range overflow, ...)!


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors


;//////////////////////////////////////////////////////////////
;End Of File
