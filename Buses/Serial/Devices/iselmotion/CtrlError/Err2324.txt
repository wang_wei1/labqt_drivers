;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 2001. All rights reserved.
;**************************************************************
;Facility: FAC_CAN_DLL

0-2324-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2324-1: Fehlerhafte Initialisierungsdatei!
3-2324-2: Dll noch nicht initialisiert!
3-2324-3: Dll bereits initialisiert!
3-2324-4: Nicht genug Systemresourcen!
3-2324-5: Nicht verf�gbare Eigenschaften!
3-2324-6: Fehlerhafte Aufrufsparameter!
3-2324-7: Caninterface nicht zul�ssig!
3-2324-8: Modus vom Cankernel nicht zul�ssig!
3-2324-9: Cankernel nicht vorhanden!
3-2324-10: Kernel�berwachungsprogramm nicht vorhanden!
3-2324-11: Anzahl der CNC-Betrieb benutzten Applikationen zu gro�!
3-2324-12: CNC-Betrieb bereits initialisiert!
3-2324-13: Dll-Bahnbuffer-Fehler!
3-2324-14: Falsche Softwareversion!


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors


;//////////////////////////////////////////////////////////////
;End Of File
