;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_IML4CTRL

0-2316-0: No error!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors
3-2316-1: 1: The transferred number is erroneous!                                                 
3-2316-2: 2: At least one hardware limit switch is active, a reference run is required!           
3-2316-3: 3: Invalid axis specification!                                                          
3-2316-4: 4: No axes specified, Reset is required!                                                
3-2316-5: 5: Syntax error: Unknown or erroneous command!                                          
3-2316-6: 6: Memory overflow! Amount of transferred data exceeds the size of available memory.    
3-2316-7: 7: Invalid number of parameters!                                                        
3-2316-8: 8: The transferred CNC command is incorrect!                                            
3-2316-9: 9: Common machine error: Power fail, Emergency Switch pressed, cover is open, ...)!
3-2316-10: A: Error code not used by IML4 control!
3-2316-11: B: Error code not used by IML4 control!
3-2316-12: C: CR expected: The interface card expected the character CR, but found another character. In most cases the number of given parameters is incorrect.
3-2316-13: D: Invalid velocity. Even axes with movement length 0 expect a valid velocity specification.
3-2316-14: E: Error code not used by IML4 control!
3-2316-15: F: User stop active!
3-2316-16: G: The given data field is invalid!
3-2316-17: H: The cover is open, could not execute the given command!
3-2316-18: =: Error code not used by IML4 control!

;//////////////////////////////////////////////////////////////
;Common Section

;Warnings

;Errors

;//////////////////////////////////////////////////////////////
;End Of File
