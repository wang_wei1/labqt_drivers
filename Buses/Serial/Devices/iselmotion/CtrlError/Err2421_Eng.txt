;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 2002. All rights reserved.
;**************************************************************
;Facility: FAC_IOWARRIOR

0-2421-0: Kein Fehler!

;//////////////////////////////////////////////////////////////
;Specific section

;Warnings

;Errors

;Common Errors for USB devices 
3-2421-1: Device error! No USB Device connected to the USB bus!
3-2421-2: Could not load communication Dll "IowComm.dll".
3-2421-3: Could not load driver Dll "IowKit.dll".
3-2421-4: The requested device could not be found. Please check the modul settings.

;Error for USB IO						
3-2421-256: No USB IO connected!
3-2421-257: The USB IO Dll is not yet initialized!
3-2421-258: The USB IO Dll is already initialized!
3-2421-259: The USB IO is already used by another application!
3-2421-260: Error during open an USB IO device!

3-2421-288: Invalid local port number! The given port number is not used by this IO module.
3-2421-289: Invalid local bit number! The given bit number is not used by this IO module.
3-2421-290: The requested binary user input is disabled within the IO module.
3-2421-291: The requested binary user output is disabled within the IO module.
3-2421-292: The requested analog user input is disabled within the IO module.
3-2421-293: The requested analog user output is disabled within the IO module.

3-2421-304: Error during an USB IO input or output operation.
3-2421-305: Error during a USB IO read operation.	
3-2421-306: Error during a USB IO write operation. 

;Error defines for USB JS						
3-2421-512: No USB Joystick connected!!
3-2421-513: The USB Joystick Dll is not yet initialized!
3-2421-514: The USB Joystick Dll is already initialized!
3-2421-515: The USB Joystick Dll is already used by another application!
3-2421-516: Error during open an USB Joytsick device!

3-2421-768: The LCD display is not used!
3-2421-769: The LCD display was not yet initialized!
3-2421-770: The LCD display was not yet enabled!

3-2421-848: Error during an USB Joystick input or output operation.
3-2421-849: Error during a USB Joystick read operation.	
3-2421-850: Error during a USB Joystick write operation. 



;//////////////////////////////////////////////////////////////
;Common Section

;Warnings
2-2421-4101: The module is disabled, could not execute the requested function!

;Errors
3-2421-4097: DLL not found!
3-2421-4098: Could not find essential initialization data of this IO module!
3-2421-4099: Version conflict! The IO module does not provide the requested function!
3-2421-4100: Invalid module index! The requested IO module does not exist!
3-2421-4101: The selected IO module is disabled!
3-2421-4102: The selected IO module is not yet initialized!
3-2421-4103: The IO module is busy with another function call!
3-2421-4353: File not found!                                                  
3-2421-4354: Could not open file!                                             
3-2421-4355: Could not read from file!                                        
3-2421-4356: Could not write to file!                                         
3-2421-4369: Could not find the IO module's initialization file!         
3-2421-4370: Could not open the IO module's initialization file!         
3-2421-4371: Could not read from the IO module's initialization file!    
3-2421-4372: Could not write to the IO module's initialization file!     
3-2421-4373: Incorrect version of the IO module's initialization file!   
3-2421-4386: A temporary file could not be opened!                            
3-2421-4387: Could not read from a temporary file!                            
3-2421-4388: Could not write to a temporary file!                             
3-2421-4401: Invalid file name!                                               
3-2421-4609: User break! The last executed action was aborted by user request.                 
3-2421-4610: Hardware error!                                                                   
3-2421-4611: Power fail!                                                                       
3-2421-4625: The IO module is locked by the security circuit module!                             
3-2421-4641: Could not start a worker thread!                                                  
3-2421-4642: An invalid index value was used!
3-2421-4643: Erroneous or invalid data occured!
3-2421-4644: An invalid position was given to the control resp. erroneous positions occured!
3-2421-4645: A process variable could not be created!
3-2421-4646: Unknown function: Either the requested function is unknown within the module DLL or the version of the used DLL is too old!
3-2421-4647: NULL Pointer: A invalid pointer was given!
3-2421-4648: Invalid velocity: The given value is either negative or too big!
3-2421-4649: Invalid speed: The given value is either negative or too big!
3-2421-4650: Invalid port number: The given value is either negative or too big!
3-2421-4651: Invalid bit number: The given value is either negative or greater than 7!
3-2421-4865: RS232 error: Could not establish the serial connection to the IO hardware!
3-2421-4866: RS232 error: Could not open the serial port!                                      
3-2421-4867: RS232 error: Could not set the parameters for serial transmission!                
3-2421-4868: RS232 error: Could not release the serial port!                                   
3-2421-4869: RS232 error: Missing serial connection to the IO module!                
3-2421-4870: RS232 error: Error during sending of data!                                        
3-2421-4871: RS232 error: Error during receiving of data!                                      
3-2421-4872: RS232 error: Timeout error during RS232 communication!                            
3-2421-5121: The cover of the machine is open!
3-2421-5122: The cover of the machine is closed!
3-2421-5137: The spindle is still switched on!
3-2421-5138: The spindle is switched off!
3-2421-5139: Could not set the requested turn direction!
3-2421-5153: The machines axes are not at their home position!
3-2421-5154: The machines axes are at their home position!
3-2421-5155: The machines axes are not at their park position!
3-2421-5156: The machines axes are at their park position!
3-2421-65535: Unknown error!                                                                   

;//////////////////////////////////////////////////////////////
;End Of File
