;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 1999. All rights reserved.
;**************************************************************
;Facility: FAC_FIELDBUS

;//////////////////////////////////////////////////////////////
;Specific section

0-2565-0: Kein Fehler!

2-2565-1: Warnung vom CAN-Bus-Verwalter: CAN-Busschnittstelle ist offline


/////////////////////////////////////////////////////////////////////////////
// ERRORS

3-2565-1: CAN-Slotkarte nicht gefunden
3-2565-2: unbekannter Typ der CAN-Slotkarte 
3-2565-3: falsche Knotenadresse: erlaubt sind Werte von 1 ... 31 
3-2565-4: zu viele CAN-IO-Ger�te (Knoten/Nodes), max. 4 beliebige Nodes erlaubt
3-2565-5: zu viele CAN-Spindel-Ger�te (Knoten/Nodes), max. 4 beliebige Nodes erlaubt
3-2565-6: es konnte kein Modul (z.B. I/O-Modul, HF-Umrichter) am CAN-Bus erkannt werden
3-2565-7: die Treibersoftware f�r das ISA-CAN-Board konnte nicht deaktiviert werden
3-2565-8: der CAN-Controller auf dem ISA-Board konnte nicht gestartet werden
3-2565-9: der CAN-Bus-Status konnte nicht exakt ermittelt werden
  
3-2565-16: Sende-Timeout: auf den CAN-Bus kann nicht zugegriffen werden 
3-2565-17: Empfangs-Timeout: das CAN-Modul mit der Knotenadresse %s antwortet nicht 
  
3-2565-32: die Sende-Warteschlange konnte nicht konfiguriert werden
3-2565-33: die Empfangs-Warteschlange konnte nicht konfiguriert werden
3-2565-34: es konnte kein Empfangs-Puffer reserviert werden
3-2565-35: f�r CAN-Modul (Knotenadresse %s) ist kein Empfangspuffer (Rx-Buffer) reserviert
  
3-2565-48: kein Timer verf�gbar
  
3-2565-65: kein Empfangs-Puffer f�r das CAN-I/O-Ger�t 1 verf�gbar (ASSIGN)
3-2565-66: kein Empfangs-Puffer f�r das CAN-I/O-Ger�t 2 verf�gbar (ASSIGN)
3-2565-67: kein Empfangs-Puffer f�r das CAN-I/O-Ger�t 3 verf�gbar (ASSIGN)
3-2565-68: kein Empfangs-Puffer f�r das CAN-I/O-Ger�t 4 verf�gbar (ASSIGN)

3-2565-69: kein Empfangs-Puffer f�r das CAN-Spindel-Ger�t 1 verf�gbar (ASSIGN)
3-2565-70: kein Empfangs-Puffer f�r das CAN-Spindel-Ger�t 2 verf�gbar (ASSIGN)
3-2565-71: kein Empfangs-Puffer f�r das CAN-Spindel-Ger�t 3 verf�gbar (ASSIGN)
3-2565-72: kein Empfangs-Puffer f�r das CAN-Spindel-Ger�t 4 verf�gbar (ASSIGN)

3-2565-80: CAN-Administrator: ben�tigte Funktionen f�r das ISA-CAN-Interface nicht gefunden
3-2565-81: CAN-Administrator: ben�tigte Funktionen f�r das PCI-CAN-Interface nicht gefunden
3-2565-82: CAN-Administrator: ben�tigte Funktionen f�r den CAN-Dongle nicht gefunden

;//////////////////////////////////////////////////////////////
;Common Section

3-2565-4097: DLL %s nicht gefunden!
3-2565-4098: Notwendige Initialisierungsdaten wurden nicht gefunden!
3-2565-4099: Falsche Version einer verbundenen DLL!
3-2565-4100: Ung�ltiger Modul-Index!
3-2565-4101: Das angeforderte Modul ist deaktiviert!
3-2565-4102: Das angeforderte Modul wurde bisher nicht initialisiert!
3-2565-4103: Das Modul ist bereits mit einer weiteren Anforderung besch�ftigt!
3-2565-4353: Datei nicht gefunden!
3-2565-4354: Datei kann nicht ge�ffnet werden!
3-2565-4355: Aus der Datei kann nicht gelesen werden!
3-2565-4356: In die Datei kann nicht geschrieben werden!
3-2565-4369: Die Initialisierungsdatei wurde nicht gefunden!
3-2565-4370: Die Initialisierungsdatei kann nicht ge�ffnet werden!
3-2565-4371: Aus der Initialisierungsdatei kann nicht gelesen werden!	
3-2565-4372: In die Initialisierungsdatei kann nicht geschrieben werden!
3-2565-4373: Falsche Version der Initialisierungsdatei!
3-2565-4386: Tempor�re Datei kann nicht ge�ffnet oder erstellt werden!
3-2565-4387: Aus einer tempor�ren Datei kann nicht gelesen werden!
3-2565-4388: In eine tempor�re Datei kann nicht geschrieben werden!
3-2565-4401: Ung�ltiger Dateiname!
3-2565-4609: Abbruch durch den Anwender!
3-2565-4610: Hardware-Fehler!
3-2565-4611: Fehler bei der Spannungsversorgung!
3-2565-4612: Der PC ist zu langsam!
3-2565-4625: Modul wird durch Sicherheitskreis gesperrt!
3-2565-4641: Ein Arbeits-Thread konnte nicht gestartet werden!
3-2565-4865: Die serielle Verbindung konnte nicht hergestellt werden!
3-2565-4866: Die serielle Schnittstelle kann nicht ge�ffnet werden!
3-2565-4867: Einrichtung der Parameter der seriellen Schnittstelle fehlgeschlagen!
3-2565-4868: Die serielle Schnittstelle kann nicht freigegeben werden!
3-2565-4869: Serielle Verbindung fehlt!
3-2565-4870: Fehler beim Senden von Daten!
3-2565-4871: Fehler beim Empfang von Daten!
3-2565-4872: Timeout-Fehler beim Betrieb der seriellen Schnittstelle!
3-2565-5121: Die Abdeckhaube ist ge�ffnet!
3-2565-5122: Die Abdeckhaube ist geschlossen!
3-2565-5137: Die Antriebsspindel ist noch in Betrieb!
3-2565-5138: Die Antriebsspindel ist ausgeschaltet!
3-2565-5139: Die angeforderte Drehrichtung kann nicht eingestellt werden!
3-2565-5153: Achsen befinden sich nicht an der Homeposition!
3-2565-5154: Achsen befinden sich an der Homeposition!
3-2565-5155: Achsen befinden sich nicht an der Parkposition!
3-2565-5156: Achsen befinden sich an der Parkposition!
3-2565-65535: Unbekannter Fehler!

;//////////////////////////////////////////////////////////////
;End Of File
