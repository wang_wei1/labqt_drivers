;**************************************************************
;Error messages for CheckErr.DLL
;Do not modify this text file!
;(c) isel Automation KG 2008. All rights reserved.
;**************************************************************
;Facility: FAC_AXCTL

0-2422-0: No error!

;//////////////////////////////////////////////////////////////
;Specific section


;//////////////////////////////////////////////////////////////
;Common Section

;Warnings
2-2422-4101: The module AxCtl.DLL for the control of independent axes (handling axes) is disabled, the required function could not be executed!

;Errors
3-2422-1: General, unspecified error! Try the desired operation after a restart of the application program and re-initializing again.
3-2422-2: Error(s) occured during an operation! In general, a SDO or PDO communication was not possible.
3-2422-3: Timeout error while waiting for an event (function axctl_WaitForEvent()). Please refer to our support.
3-2422-4: At least one axis is in a bad condition. In general, a reset is required or a limit switch must be releaved or the power supply is not switched on.
3-2422-5: Reference run error! There was still no reference run for the requested axis performed.
3-2422-6: Invalid handle or invalid axis index. The requested axis does not exist or is not used.
3-2422-7: DLL not initialized! There was still no successful initialization with the function "initialize handling axes" performed.
3-2422-8: DLL already initialized! The function "initialize handling axes" has already been executed.
3-2422-9: Error while reading the axis configuration from the handling axes configuration file.
3-2422-10: Improper operation mode! When speed control is active positioning is not possible or vice versa. (Subject functions axctl_MoveAbs(), axctl_MoveRel()).
3-2422-11: Could not open the initialization file!
3-2422-12: Error while opening the serial interface!
3-2422-13: Invalid parameters! (Structure size varies). This indicates the use of an outdated version of the library AxCtl.DLL.

3-2422-201: CanAPI.DLL not found or not initialized. For the use of the module for the operation of handling axes (AxCtl.DLL), a CAN controller DLL (CANAPI.DLL) must be used.

3-2422-4097: The module AxCtl.DLL for the control of independent axes (handling axes) was not found! This library should be located in the same directory as the CAN-Control DLL CanAPI.DLL, e.g. \CNCWorkbench\Control\CAN.
3-2422-4098: Could not find essential initialization data!
3-2422-4099: Wrong version of the module AxCtl.DLL! An attempt was made to use a function for the control of a handling axis which requires a newer release state of AxCtl.DLL.
3-2422-4101: The module AxCtl.DLL for the control of independent axes (handling axes) is disabled, the requested function was blocked!
3-2422-4102: The module AxCtl.DLL was not yet initialized!
3-2422-4103: The module AxCtl.DLL is busy executing another function!
3-2422-4386: Could not create/open a temporary file!
3-2422-4387: Could not read from a temporary file!
3-2422-4388: Could not write to a temporary file!
3-2422-4401: Invalid file name!
3-2422-4609: Aborted by user!
3-2422-4641: Could not start a worker thread!
3-2422-4642: An invalid index value was used!
3-2422-4643: Erroneous or invalid data occured!
3-2422-4644: An invalid position was given to the control resp. erroneous positions occured!
3-2422-4645: A process variable could not be created!
3-2422-4646: Unknown function: Either the requested function is unknown within the module DLL or the version of the used DLL is too old!
3-2422-4647: NULL Pointer: A invalid pointer was given!
3-2422-4648: Invalid velocity: The given value is either negative or too big!
3-2422-4649: Invalid speed: The given value is either negative or too big!
3-2422-65535: Unknown error!

;//////////////////////////////////////////////////////////////
;End Of File
