RSRC
 LVCCLBVW  P<  �      P   � �0          < � @�      ����            �E`���D�e���          Y��K��ZN���N@�L\��ُ ��	���B~       T�bQA_�B�nh���   �5�x�7;����/�   �  11.0.1            � LVCCFPGA_I2C IO Cluster Dynamic.ctl      VICC     FPGA_I2C IO Cluster.ctlPTH0      FPGA_I2C IO Cluster.ctl                                  8   4Specifies the FPGA IO items for the I2C port to use.   ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������   � FPHPFPGA_I2C IO Cluster Dynamic.ctl      TDCC     FPGA_I2C IO Cluster.ctlPTH0      FPGA_I2C IO Cluster.ctl                                   �  �  �  �PTH0              � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       �  5�x�͛ol�ǟ��y���4�ԉc�I�i���������������Ҏ��^C�
{�t�7�M����Z��){�^�Eߠ)�	�UUЦ4�t���UK[:r�����]���}v��(�1ϟ������RZ�[��Ϣ�ĉ7����i��^�^� wq���} 8�g�N5����Z�8�47��ʟ��P���|
j�����RF��tJ�i~I?Z.��e�Ǥ��jjvC����岈�W�����B"%i�z{����WA��nNJ�;aE��O���)>[�D�d:{��9�oL��oc�����ZbN+��2��9>}�s5��ξ�ݻ6�>�v)3a���ɤ�[�a��i�

H��BH��B�w���8�M=%�ϩ/�Y���V}l+�!��c�>���Ri�iD��:�
~:�W�䫰��mӫ�����,CO�2�d��a�����D���r�{���ڲF�!¯F���:�N�>�
Ws���2�y~8���h�h��i��gt7�Q�q݋��j�����I���+��e�zQ���kW��ٷ�o����������z9��%[ߢC|�l���1�ҷA8�th�9����E}�}z��,շ�hk�`~�B�<y�޸�)�h_�qF���U����ԋ|
%�&���8EJG`#nB����n�8Wz)ߵ����m��m����z[�z��@�}��c
����	o�5$z��
}_k���@R?���[�/�.T��J1A�E#���6���^�X%	�W�:A�N�-�:W��f�?+/����'^��nt�t����x����s�i��Ücey��*�E�[�:[�����lv�������4������~_]���bd�E?�9�Ch�:D<H�&���g+��
ͦ�� 4v廠���Gնo��5*�F:�N����f�d�P;���;�_i;���Ryù�x�x:�-�h���`�X�tU�~�����p5�$��=%i�x{�s�ܚM���:Ò�4ΦI[â�PS�(
w�#Sa��AԐ�hoR+�!bW�vǅ.���j�/Zx6���r����8�g�5�kt�D�y���efÕd�C����*&���L�q/�q�����*���/~����j�/f���ly��������ݷ'�ѨD�{��o��xh-��J��B216:�Jx�%�L�8�|�۟=r �H���i�Hb$q��蚃G�v�y���nlDs��̙�('�|T�;*%�%?8�`1)WF
��-R��B��)�3fR�猤�/�I��5 �c!�D
��@J�����q!����1D�y��_ϼ�['�s� �ڄ���M\��"W#_9�Y(�j$벫�����q�浸ZD����Fʻ��}Wk`q5��ռ��ct�����:��1#� �%{uU���0���%Pw��:I��ZŚ@w�H��$�ϒ@ĭZ�@������\P����j��p���*��^'��I�`%%T�����d!��E�rR�]�- 孼��fs5|�p54	����삫}��ն�p��*p��n�Z�����Fqp���Ȉ~}��j�����ݮ��1����F�]�<��jdO�����j6W##�R�`�&[���jd��"?q+�H��"c��@�0s�q�"G���A���@�	�/�y�e���U��Ⱦ
Iy�5R�W��gꄔ!vR⬤$J�2\RZ-�4�E�Rnwr5�H�λZ��j�s��I�A�6W�W\p�uE����]�������j����FW�8�Z����+�^p5<[���]w5�W�s_����̮�?bs5�qyW;�6gw5ތ��a�=�Hu\�V�@�%�P���I����5��J$PK�ݒ@�[	$:$P��j��	�$���yu\Q)�K�H��o�)�+fR�I���I����a!��)Xu %��j'��yW[fw�������6�]�\�bW[W��6T�j�v�ﲻ��X]�~W���j�I��g���]m�������dW���j�]��]mZ�E�6W�O�������WW�OU�@�]K�g��@Cu�@q�J�&ЏK$Ё$P�%�|n%�s	��t��(( ��o�*���
I�w��]U ew��c'e�����<VRVXHit���H	;�ڂ�e��]�x ʽ��$���i������Km'n��PP���������;���V�M�NA���~qjjGaf�]�%Z+�:���1�         . BDHPFPGA_I2C IO Cluster Dynamic.ctl            e   ux�c``(�`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-���W)b��z�\�8Se�<� e�            1      NI.LV.All.SourceOnly �     !                     	   (                                        �                    �     @UUA N 	                                                   ��*>  �>��*>  �>     @   ?          I  �x��Ϗ�@ǟ@d����`℣!v��R�E*�q]l�hJ�1��Ì	{�/�����?A�����"���м������}�k�Gx�]����+ pJ�	��w��c6B�H*p�!-��N�p���'t4�ܭ"����?��q0��y�jq��Pp�o��LL��uǃE�������2�{J� "�xt&QwI":F��0�K_��
S�T�Β;��"��nKpZ?��)���@�f��$����0:�1����xZO�D���ހc/�svb]b���e���`�O��b��!��c�0)��w��n�c��
>y����H�O���U�������#LzEh�رף����
��<W�6��h�!�������uۿ4�%�c��8F8����g�+���1N�������*R/�c���j��N�Y�]h���fe9����r[�4��p�gL�k�� ���Ӈ/���m�~�7ڒ@�H��� ��8�AAr뉘��L#�0��������so����k�P�w%kAQ:i���Fǿ��պ	ugG�*%����Wa��]ʩx/[�����? ��      e       H      � �   Q      � �   Z      � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW  P<  �      P               4  �   LVSR      RTSG       vers      4CONP      HLIvi      \STRG      pICON      �icl8      �LIfp      �STR    	  �FPHb      �FPSE      �LIbd      �BDHb      �BDSE      �VITS      �DTHP       MUID      HIST      (PRT       <VCTP      PFTAB      d                        ����       �       ����       �        ����       �        ����       �        ����      X        ����      �        ����              ����             ����      �       ����      �       ����      �       ����      �       ����      �       ����      $�       ����      *�       	����      0�       
����      6�       ����      <�        ����      B�        ����      K�        ����      K�        ����      K�        ����      L`        ����      Lh        ����      L�        ����      L�        ����      L�        ����      L�        ����      M`       �����      O�    FPGA_I2C IO Cluster Dynamic.ctl