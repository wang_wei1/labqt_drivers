RSRC
 LVCCLBVW  L  %      ,   � �0          < � @�      ����            �(�8řF�7`����          5�>}m�eE�7�Y�(_A��ُ ��	���B~       ��F��!�E��<�����   7k��ˆ��`�'�   �  11.0.1            ( LVCCFPGA UART IO Cluster.ctl           8   4Specifies the FPGA IO items for the I2C port to use.   ������  �  �@ �� !�� a�  !�� !�� !�� !�� !�� �� �� �� �� �� �� �� ��0��,�����$���.9��$�� ���,��0�@ �  �  ����   ���������������������������������                              ��                              ��        +                     ��       +��               �    ��      +����             ��    ��   ��V                   �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ���               �    ��       ��� ��                 ��   ��V ���Vu�                 ��      +�P&Ju�+                ��     V��Jutu�+                ��      ����uv�+                ��      �����ʬ+                ��       ����V�+                ��   ��V ���  ++                ��       ���       ��+          ��       Ь�       ���+        ��       ���     ##���+      ��       ���       ����+    ��       ���       �����##  ��   V�V ���       ����+    ��               �����+      ��      +����      ���+        ��       +��       ��+          ��        +                     ��                              ��                              ���������������������������������   ( FPHPFPGA UART IO Cluster.ctl          � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       � �               displayFilter �                   tdData �               IOInterface �     0����     C<Interface>
<MethodSet>
   <Method name="Read">
      <AttributeSet>
         <Attribute name="NumberOfSyncRegistersForRead">
            <LocalizedName>Number of Synchronizing Registers for Read</LocalizedName>
            <LocalizedValues>Auto,0,1,2</LocalizedValues>
            <SupportedValues>InheritFromProjectItem,Auto,0,1,2</SupportedValues>
            <Value>Auto</Value>
         </Attribute>
      </AttributeSet>
      <ReturnValue>
         <Type>bool</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Data">
      <ParameterList>
         <Parameter name="Data">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Set Output Enable">
      <ParameterList>
         <Parameter name="Enable">
            <Direction>in</Direction>
            <Required>yes</Required>
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
      <ReturnValue>
         <Type>void</Type>
      </ReturnValue>
   </Method>
   <Method name="Write">
      <ParameterList>
         <Parameter name="Value">
            <Type>bool</Type>
         </Parameter>
      </ParameterList>
   </Method>
</MethodSet></Interface>       	typeClass �     0����      FPGA I/O       F  Sx��VMLe~��Y����R�6����ڭM�RI�2��
�]9�6�M�)�fq+�PC&4�R�`�艫^�Y�d�4јؚlڃg5�`"���}3�3�.[SѸ�ɗ�}޿�y��uL�'T`�"��pڄ`�  ��#M�?�t��	g�tG��Bi��Ֆ�W'ThnHw1�E
c��	�i�[QJ��y��쯦��t�T�i%�E��9��A���D���?�FFW��@��yʀ	�f�+�'1#�����HK;)S���5$۠^��qĐ2ow�	�1�J�0�9FX�#r���`�3P=�q͐��gd���̓qY:��)�wӷ�mg�/� )��֧�"�ˣ�ہD��DJ�9i�S�!�"��O�{�!�A��pi C����,��C_s�-��4��Ef�oe�R�Ќ@�e�հD�����`uu'Ƨ=��n�T�ɘ�2�Y[q�]���\���3����Ȗ��\���+�y�U�1T�,��w�Z��}�>�"Z�S+da�6Q��6�U+�\�lLb�yՊ�,b�>\��P+�֩uee�^�/�(�5+bkV�em[�L�?X��(d�r��DjtC��b}oݳ�a����0�P�y�x�Z�V-���c�#�u�ک�"g�"hǐC8)���ǵ�0��<���l1s����.H���{g��
��}D����LQ����?<�����Z2_�eY��������R��@BY:yC&�4��>m�'���0�~9ۨu�#6�ݰG���c!R�i�@�� CY��Ck�n&5Ï�'v����JSy'ɽZg{���jng�Oym���Y�jF�_)�۽P{�"J&�y~��36Px�3������.w[_yΛ��=3�z�g�T�W�:%l|{h<��Ql.��>����
���P��\��\�r`<��r)���o|������]��[X�M�@3ށ~��Ο�+'\�l[�@��ʋ��+�r�oz�Ԯye�_��?���{e�Q���^9�x���o��2��+-Xs�����,��4�h��L��MS��91�,^�B�Lثt�K��E��t�x��7i��c��[<]C�P��(�D辿 �u#_        <   ( BDHPFPGA UART IO Cluster.ctl           e   ux�c``(�`��P���I�+�!���YЏ�7���a �( 	����.��>��� �l���9�2-���W)b��z�\�8Se�<� e�            1      NI.LV.All.SourceOnly �     !                     h   (                                        �                    �     @UUA N 	                                                   ��*>  �>��*>  �>     @   ?          �  ax��T͎�0
�V���8Y+q�Ȃ�8��v)��l���\�d�5J��k#uO��8<���' �M�&d�X���e�|�7��� @�{/~���� ���]�\�Q��9M�Azp`I�=�"��oQ-D:A�	!�p��W�1�t�Ƌ�P)�fZ�܌��S�����5Ob<c+Sl5��VrODB3v�驡&sRpRpv��q*d.$)t|�μF�=�4���Vbp0x:x��+su�D繐�eC�@��H��X�O��P�r�+ؤ�m�6�{���s��Ι쵹�Ǩ��������٭i�}o�-olʐH�\+rL��{L��O���U]�T��J�1��+&x���U�:*�smRi��U�ty�������k��zkۿ5�`�1�5��o`a�������d71Ί7}���V�nw���{��,��h�fH���p��9y����\˔���#�1'6������ߊ�C�}7���@�2]��'�����:���W	���=�m�l�t~���L     e       H      � �   Q      � �   Z      � �   c� � �   � �Segoe UISegoe UISegoe UI0   RSRC
 LVCCLBVW  L  %      ,               4     LVSR      RTSG       vers      4CONP      HLIvi      \STRG      pICON      �icl8      �LIfp      �STR      �FPHb      �FPSE      �LIbd      BDHb      $BDSE      8VITS      LDTHP      `MUID      tHIST      �PRT       �VCTP      �FTAB      �                        ����       �       ����       �        ����       �        ����       �        ����       �        ����      $        ����      �        ����      �       ����      �       ����      �        ����      �        ����      $        ����      ,        ����      X        ����      �        ����      �        ����              ����              ����              ����      @        ����      �       �����      �    FPGA UART IO Cluster.ctl